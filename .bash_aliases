#!/bin/bash

## DOWNLOADED FROM GIT REPO

# My Alias

# ffmpeg Customs
alias mp4-mp3-folder='for vid in *.mp4; do ffmpeg -i "$vid" -f mp3 -ab 320000 "${vid%.mp4}.mp3"; done'
alias mp4-mp3='mp4mp3(){ ffmpeg -i "$1" -f mp3 -ab 320000 "${1%.mp4}.mp3"; unset -f mp4mp3;}; mp4mp3'
alias mkv-mp4-folder='for vid in *.mkv; do ffmpeg -i "$vid" -vcodec copy -acodec copy "${vid%.mkv}.mp4"; done'
alias mkv-mp4='mkvmp4(){ ffmpeg -i "$1" "${1%.mkv}.mp4"; unset -f mkvmp4;}; mkvmp4'
alias mkv-mp3-folder='for vid in *.mkv; do ffmpeg -i "$vid" -vn -c:a libmp3lame -y "${vid%.mkv}.mp3"; done'
alias mkv-mp3='mkvmp3(){ ffmpeg -i "$1" -vn -c:a libmp3lame -y "${1%.mkv}.mp3"; unset -f mkvmp3;}; mkvmp3'

# When you get bored
alias telnet-starwars='telnet towel.blinkenlights.nl'
alias nethack-online='ssh nethack@nethack.alt.org ; clear'
alias tron-online='ssh sshtron.zachlatta.com ; clear'

# My IP Info
alias myip='MYIP=$(curl -s https://ipecho.net/plain; echo) && curl https://ipinfo.io/"$MYIP" && echo'
alias ports-in-use='sudo lsof -i -P -n | grep LISTEN'

# Typo
alias whosi='whois'
alias gti='git'
alias dc='cd'

# Custom
alias untar='tar xvf'
alias nano='vim'
alias nanoo='/usr/bin/nano'
alias svim='sudo vim'
alias del='shred -f -n 30 -z -u -v'
alias del-quick='shred -f -n 3 -z -u -v'

#amass
alias amass-subdomain='domain(){ /opt/amass/amass enum -passive -d "$1" -r 1.1.1.1 9.9.9.9 8.8.8.8 >> /opt/amass-scans/"$1".txt; unset -f domain;}; domain'

#SAV Command
alias sav-start='sudo service sav-protect start'
alias sav-stop='sudo service sav-protect stop'

# wget Shortcuts
alias get-nordserverlist="wget https://downloads.nordcdn.com/configs/archives/servers/ovpn.zip"
alias get-wp6="wget https://git.ncltech.co.uk/Phil/hak5_WiFi_Pineapple_NANO_Tools/raw/commit/b97f2cdae7d0df34f659f199b5a71e448f2f79d5/wp6.sh"

# Hack the Box
alias htb-vpn-starting_point='sudo openvpn ~/CTF/HTB/vpn/starting_point_NCLtech.ovpn'
alias htb-vpn-lab='sudo openvpn ~/CTF/HTB/vpn/lab_NCLtech.ovpn'
alias htb-folder='cd ~/git/CTF/HTB/'

# TryHackMe
alias thm-vpn='sudo openvpn ~/CTF/THM/vpn/NCLtech.ovpn'
alias thm-network-wreath='sudo openvpn ~/CTF/THM/vpn/NCLtech-wreath.ovpn'
alias thm-folder='cd ~/git/CTF/THM'

# Enable aliases to be sudo’ed
alias sudo='sudo '

# View HTTP traffic
function sniff() {
  sudo ngrep -d $(ip route | grep '^default' | awk '{print $5}') -t '^(GET|POST) ' 'tcp and port 80'
}
function httpdump() {
  sudo tcpdump -i $(ip route | grep '^default' | awk '{print $5}') -n -s 0 -w - | grep -a -o -E \"Host\\: .*|GET \\/.*\"
}
# Start-msfconsole
function mfsconsole-start(){
if [ -d "/opt/sophos-av" ];then
    	    sudo service sav-protect stop >> /dev/null
    	    msfconsole
    	else 
    	   msfconsole
   fi
}

# nmap-basic
function nmap-initial() {
  if [ -d "./nmap" ];then
			sudo nmap -sC -sV -oA ./nmap/initial $1 -v
	else
			mkdir ./nmap
			sudo nmap -sC -sV -oA ./nmap/initial $1 -v
fi
}

# nmap-basic
function nmap-allports() {
  if [ -d "./nmap" ];then
			sudo nmap -sC -p- -oA "./nmap/$1-allports" $1 -v
	else
			mkdir ./nmap
			sudo nmap -sC -p- -oA "./nmap/$1-allports" $1 -v
fi
}
# Wireshark Shortcuts

alias wireshark-pfsense="sudo su -c 'wireshark -k -i <(ssh root@192.168.0.3 -p 2222 tcpdump -i lagg0 -U -w - )'"
alias wireshark-pfsense-lab="sudo su -c 'wireshark -k -i <(ssh root@10.74.85.11 -p 2222 tcpdump -i xn0 host not 192.168.120.27 -U -w - )'"

# Git Functions
function gpm() {
  if [ "$(grep -c "[branch "master"]" .git/config )" -gt 0 ]; then
   git push origin master
  elif [ "$(grep -c "[branch "main"]" .git/config )" -gt 0 ]; then
   git push origin main
  else
   echo "fatal: not a git repository (or any of the parent directories): .git"
fi
}

function gac() {
if [ -d ".git" ]; then
echo "Log Message: "
read logmessage
git add --all && git commit --allow-empty-message -m "$logmessage"
else
echo "No .git Folder found in Directory. Are you in the Correct Folder?"
fi
}


# Pull all Git repos in this dir

function pullall() {
  for dir in * ; do
    # Execute in subshell
    (
      cd "$dir" || exit 1
      # If it's a git directory, then update it
       if test -d .git; then
	# Check if its Master or Main
        if [ "$(grep -c -i ""master"" .git/config )" -gt 0 ]; then
         echo "$dir is a Git repo. Pulling master branch..." && git checkout master && git pull && echo ""
       
      elif [ "$(grep -c -i ""main"" .git/config )" -gt 0 ]; then
        echo "$dir is a Git repo. Pulling main branch..." && git checkout main && git pull && echo ""
               fi
      else
        echo "$dir is not a Git repo." && echo ""
      fi
    )
 done
}

# Pull Dotfiles inside ~/git/dotfiles
function pulldots() {
           
        # Check if folders are there and if its Master or Main
        if [ "$(grep -c -i ""master"" ~/git/dotfiles/.git/config )" -gt 0 ]; then
          CURRENTDIR=$(pwd) && echo "dotfiles Found, Pulling master branch..." && cd ~/git/dotfiles && git checkout master && git pull && cd $CURRENTDIR && echo ""
        
        
        elif [ "$(grep -c -i ""main"" ~/git/dotfiles/.git/config )" -gt 0 ]; then
           CURRENTDIR=$(pwd) && echo "dotfiles Found, Pulling main branch..." && cd ~/git/dotfiles && git checkout main && git pull && cd $CURRENTDIR && echo ""
          
     else 
         echo "I can't seem to file any dotfiles folders" && echo ""
      fi      
}

## TEST FIX

# Updates packages for all Distros
function update() {
sudo -v
	
   # if command -v snap >/dev/null; then
   # echo "Updating Snaps..."
   # sudo snap refresh
    if [[ -f "/usr/bin/snapctl" ]];then
	    echo "Updating Snaps..."
	    sudo snap refresh
	else
	 echo "No Snap Found"
  fi
if [ "$(grep -c "parrot" /etc/os-release)" -gt 0 ]; then
    echo "Updating Apt Packages..."
    	  if [ -d "/opt/sophos-av" ];then
    	    sudo service sav-protect stop >> /dev/null
    	    sudo parrot-upgrade -y && sudo apt full-upgrade -y
    	    sudo service sav-protect start >> /dev/null
    	else 
    	   sudo parrot-upgrade -y && sudo apt full-upgrade -y
	
  fi
elif [ "$(grep -c "kali" /etc/os-release)" -gt 0 ]; then
    echo "Updating Apt Packages..."
    	  if [ -d "/opt/sophos-av" ];then
    	    sudo service sav-protect stop >> /dev/null
    	    sudo apt update && sudo apt --autoremove full-upgrade -y
    	    sudo service sav-protect start >> /dev/null
    	else 
    	    sudo apt update && sudo apt --autoremove full-upgrade -y	
  fi 
elif [ "$(grep -c "debian" /etc/os-release)" -gt 0 ]; then
    echo "Updating Apt Packages..."
    	  if [ -d "/opt/sophos-av" ];then
    	    sudo service sav-protect stop >> /dev/null
    	    sudo apt clean && sudo apt update && sudo apt full-upgrade -y
    	    sudo service sav-protect start >> /dev/null
    	else 
    	    sudo apt clean && sudo apt update && sudo apt full-upgrade -y	
  fi
elif [ "$(grep -c "arch" /etc/os-release)" -gt 0 ]; then
      echo "Updating Pacman Packages"
          if [ -d "/opt/sophos-av" ];then
	         sudo service sav-protect stop >> /dev/null
	         sudo pacman -Syy -y $$ && sudo -v && sudo pacman -Syu -y && sudo pacman --noconfirm -R $(pacman -Qdtq)
	         sudo service sav-protect start >> /dev/null
	    else 
	       pacmanorphan=$(sudo pacman -Qdt)
	       sudo pacman -Syy -y && sudo -v && sudo pacman -Syu -y && sudo pacman --noconfirm -R $(pacman -Qdtq)
  fi

else
    echo "No apt or pacman found. I guess you're out of luck"
  fi
}

# Decrypt pfSence backup configs # pfdecrypt 'File-in'  'File-out'  'Password'
pfdecrypt() {
cat "$1" | sed -e '1d' -e '$d' | base64 -d | openssl enc -d -aes-256-cbc -md md5 -out "$2" -k "$3"
}

# Extract Function
extract() {
  if [ ! -f "$1" ];
  then
   printf '"%s" is not a valid file.' "$1"
   exit 1
  fi
 
  case $1 in
   *.tar.bz2)  tar xjf "$1"    ;;
   *.tar.gz)   tar xzf "$1"    ;;
   *.bz2)      bunzip2 "$1"    ;;
   *.rar)      unrar x "$1"    ;;
   *.gz)       gunzip "$1"     ;;
   *.tar)      tar xf "$1"     ;;
   *.tbz2)     tar xjf "$1"    ;;
   *.tgz)      tar xzf "$1"    ;;
   *.zip)      unzip "$1"      ;;
   *.Z)        uncompress "$1" ;;
   *.7z)       7z x "$1"       ;;
   *.deb)      ar x "$1"       ;;
   *.tar.xz)   tar xf "$1"     ;;
   *)          printf 'cannot extract "%s" with extract()' "$1" ;;
   esac
 }

# Disable ipv6
function ipv6-disable() {
sudo sysctl -w net.ipv6.conf.all.disable_ipv6=1
sudo  sysctl -w net.ipv6.conf.default.disable_ipv6=1
}

# Enable ipv6
function ipv6-enable() {
sudo sysctl -w net.ipv6.conf.all.disable_ipv6=0
sudo sysctl -w net.ipv6.conf.default.disable_ipv6=0
}

# Start CyberChef
alias cyberchef="open /opt/cyberchef/cyberchef"

# Local IP  lookup
alias ips="ip a | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'"
alias eth0="ip a s eth0 | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*'"
alias eth1="ip a s eth1 | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*'"
alias wlan0="ip a s wlan0 | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*'"
alias wlan1="ip a s wlan1 | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*'"
alias tun0="ip a s tun0 | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*'"
alias tun1="ip a s tun1 | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*'"
# Sublist3r
alias sublist3r="python3 /opt/Sublist3r/sublist3r.py"

# Get PentestMonkey Reverse php Shell
alias phpshell='wget https://raw.githubusercontent.com/pentestmonkey/php-reverse-shell/master/php-reverse-shell.php -O "shell.php"'

# Start reverse_shell_generator
alias reverse-shell-gen-start='docker start a2f132aa528a; open http://localhost:3321'
alias reverse-shell-gen-stop='docker stop a2f132aa528a'

# Wireguard - TheHomeTunnel
alias thehometunnel-up="sudo wg-quick up TheHomeTunnel"
alias thehometunnel-down="sudo wg-quick down TheHomeTunnel"

# windirstat shortcut for ncdu
alias windirstat="ncdu"
